require 'mechanize'
require_relative 'exceptions/invalid_response_error'
require_relative 'exceptions/page_nil_error'

class Page
	attr_reader :url, :assets, :page

	def initialize(options = {})
		@logger = Logger.new(STDOUT)
		@url = options[:url]
		@only_process_test_fixtures = options[:only_process_test_fixtures]
		@assets = Hash.new
		@page = get_page

		# Raise exceptions
		raise PageNilError.new(@url) if @page.nil?
		raise InvalidResponseError.new(@page) if @page.instance_of? Mechanize::File
	end

	# Grabs the links present on the page as a string array
	def get_links

		begin
			links = @page.links.map { |a| @page.uri.merge(a.uri.to_s).to_s }
			links.reject! { |link| URI.parse(link).host != @page.uri.host || link.include?('cdn-cgi') }
			return links
		rescue
			return nil
		end
	end

	# Outputs the page to a hash format: { "url": "https://www.gocardless.com", "static_assets": [] }
	def to_hash
		output = Hash.new
		output[:url] = @url
		output[:static_assets] = get_assets
		return output
	end

	# Grabs the static assets on the page (at the minute these are: css, javascript and images)
	def get_assets

		# Images
		unless @page.image_urls.nil?
			@assets[:images] = @page.image_urls
		end

		# JavaScript assets
		@assets[:js] = []
		@page.search('script[src]').each do |js|
			@assets[:js].push(relative_to_absolute(js.attributes['src'].value))
		end

		# Stylesheets
		@assets[:css] = []

		@page.search('link[rel="stylesheet"]').each do |css|
			@assets[:css].push(relative_to_absolute(css.attributes['href'].value))
		end

		# Return the fully populated hash
		return @assets
	end

	# Returns the Mechanize / Nokogiri page object
	def get_page
		agent = Mechanize.new
		begin
			return agent.get @url

		rescue Exception => e
			@logger.debug "FAILED: #{@url}"
			return nil
		end
	end

	private

	def relative_to_absolute(path)
		@page.uri.merge(URI(path).to_s).to_s
	end
end