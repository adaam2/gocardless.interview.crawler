require 'uri'
require 'logger'
require_relative 'exceptions/no_url_provided'
require_relative 'page'

class Crawler
  attr_reader :thread_pool

  # constructor
  def initialize(options = {})
    @thread_pool = []
    @queue = Queue.new
    @mutex = Mutex.new
    @logger = Logger.new(STDOUT)
    @output = []
    @max_threads = options[:max_threads]
    @root_domain = options[:root_domain]
    @only_process_test_fixtures = options[:only_process_test_fixtures]
    @start_automatically = options[:start_automatically]

    # noinspection RubyResolve
    raise NoUrlProvided unless @root_domain

    if @start_automatically
      start_crawler
      start_fetchers
    end
  end

  # provide accessor to queue var in multi threaded context
  def queue
    @mutex.synchronize do
      @queue
    end
  end

  # provide accessor to output object in multi threaded context using mutex
  def output
    @mutex.synchronize do
      @output
    end
  end

  # push the first item (the root of the domain) to the queue
  def start_crawler
    enqueue @root_domain
  end

  # start multiple threads to do the work on the items in the queue
  def start_fetchers
    1.upto(@max_threads) do
      @thread_pool << Thread.new {

        # loop until queue is empty
        until queue.empty? do
          item = queue.pop(true) rescue nil
          # Work on the item popped from the queue
            crawl item
        end
      }
    end

    @thread_pool.each(&:join)
    @logger.debug("Completed crawl of #{@root_domain}. Total URLs found: #{output.size}")
  end

  def enqueue(item)
    queue.push item
  end

  # Crawl individual url.
  def crawl(url)
    if exists_in_processed(url)
      @logger.debug "EXISTS: #{url}"
      return true
    end

    begin
      # Initialize a new page instance
      page = Page.new(:url => url, :only_process_test_fixtures => @only_process_test_fixtures)

      # Grab the child links on the current page
      links = page.get_links

      unless links.nil?
        # Add the collection of links to the queue
        links.each do |link|
          enqueue link
        end
      end

      # Grab hash to add to the final sitemap output (this includes assets enumerable)
      hash = page.to_hash
      output.push hash

      @logger.debug "NEW: #{url}"
    rescue StandardError => error
      @logger.error(error)
    end
  end

  def exists_in_processed(url)
    output.any? { |hash| compare_url(hash[:url], url) }
  end

  def compare_url(first, second)
    first_uri = URI(first)
    second_uri = URI(second)

    return remove_trailing_slash(first_uri.path) == remove_trailing_slash(second_uri.path)
  end
  
  private
  def remove_trailing_slash(path)
    return path.gsub('/', '')
  end
end