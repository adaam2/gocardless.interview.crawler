require 'test/unit'
require_relative '../page'
require 'fakeweb'

class PageTest < Test::Unit::TestCase

  def setup
    @page = Page.new(:url => 'https://www.adamjamesbull.co.uk')
    @fake_url = 'http://www.gocardless.com'
  end

  def test_page_is_not_nil
    page = Page.new(:url => @fake_url)
    assert(!page.nil?)
  end

  def test_page_is_not_nil_with_local_file

    page = get_test_page('gocardless_home', @fake_url)
    assert(!page.nil?)
  end

  def test_page_with_no_hyperlinks
    page = get_test_page('no_hyperlinks', @fake_url)

    assert !page.get_links.nil?
  end

  def test_to_hash
    page = get_test_page('gocardless_home', @fake_url)
    hash = page.to_hash
    assert(!hash[:url].nil?)
    assert(!hash[:static_assets].nil?)
  end

  def test_get_assets_with_no_hyperlinks
    page = get_test_page('no_hyperlinks', @fake_url)
    assets = page.get_assets
    assert assets[:css].size < 1 && assets[:js].size < 1 && assets[:images].size < 1
  end

  private
  def get_test_page(html_file_name_partial, web_address)
    stream = File.read("#{__dir__}/fixtures/#{html_file_name_partial}.html")
    FakeWeb.register_uri(:get, web_address, :body => stream, :content_type => 'text/html')
    page = Page.new(:url => web_address, :only_process_test_fixtures => true)
    return page
  end
end