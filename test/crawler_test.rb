require 'test/unit'
require_relative '../crawler'

class CrawlerTest < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    @crawler = get_crawler
  end

  def test_compare_url_ignores_trailing_slash
    # These two urls are the same
    assert(@crawler.compare_url('http://www.google.com', 'http://www.google.com/'))
    assert(@crawler.compare_url('http://www.google.com/about', 'http://www.google.com/about/'))
  end

  def test_queue_add_items
    1.upto(20) do |item|
      @crawler.queue.push(item)
    end
    assert_equal(@crawler.queue.size, 20)
  end

  def test_max_threads_respected
    @crawler.start_fetchers
    assert_equal(@crawler.thread_pool.count, 40)
  end

  def test_exists_in_processed
    url = 'http://google.com'
    page = Page.new :url => url
    @crawler.output.push page.to_hash
    assert(@crawler.exists_in_processed(url))
  end

  private
  def get_crawler
    root_domain = 'http://www.test.com'
    crawler = Crawler.new :root_domain => root_domain, :max_threads => 40, :only_process_test_fixtures => true, :start_automatically => false
    return crawler
  end
end