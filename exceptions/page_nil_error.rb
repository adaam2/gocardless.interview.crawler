class PageNilError < StandardError
	attr_reader :url

	def initialize(url)
		@url = url
	end

	def to_s
		"FAILED: #{url}"
	end
end