class InvalidResponseError < StandardError
	attr_reader :response

	def initialize(response)
		@response = response
	end

	def to_s
		"NOT A WEBPAGE: #{response.filename}"
	end
end