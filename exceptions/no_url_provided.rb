class NoUrlProvided < StandardError
	def to_s
		'ERROR: No root URL was provided'
	end
end