require_relative 'crawler'
require_relative 'sitemap'

root_domain = ARGV.last # i.e. https://www.gocardless.com

crawler = Crawler.new :root_domain => root_domain, :max_threads => 40, :only_process_test_fixtures => false, :start_automatically => true

sitemap = SiteMap.new(:data => crawler.output, :host => root_domain)
sitemap.generate
