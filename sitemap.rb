require 'json'

class SiteMap
	def initialize(options = {})
		
		raise Exception if options[:data].nil? || options[:host].nil?
		@data = options[:data]
		@path = options[:path]
		@host = options[:host]
	end

	def data
		@data
	end

	def generate
		File.open("public/sitemap.json", 'w') do |f|
			f.write(JSON.pretty_generate(data))
		end
	end
end